package su.sviridoff.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

    @Value("${chromedriver.screenshot.folder}")
    private String chromedriverScreenshotFolder;

    public void addResourceHandlers(ResourceHandlerRegistry handler) {
        handler.addResourceHandler("/images/**")
                .addResourceLocations("file:///" + chromedriverScreenshotFolder + "\\");
    }

}
