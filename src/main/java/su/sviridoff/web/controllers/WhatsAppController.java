package su.sviridoff.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import su.sviridoff.web.services.WhatsAppService;

import java.util.Optional;

@RestController
@RequestMapping("/whatsapp")
public class WhatsAppController {

    @Autowired
    private WhatsAppService whatsAppService;

    @GetMapping("/open")
    public String index() {
        whatsAppService.open();
        return "Opened web-site: " + whatsAppService.getTitle();
    }

    @GetMapping("/isLoggedIn")
    public String isLoggedIn() {
        whatsAppService.refresh();
        whatsAppService.humanWait(10);
        if (whatsAppService.isLoggedIn())
            return "WhatsApp service is logged in.";
        else
            return "WhatsApp service is not logged in. Auth is required in whatsapp.";
    }

    @GetMapping("/QRCode")
    public ModelAndView getCode() {
        ModelAndView model = new ModelAndView("qr-code");
        whatsAppService.refresh();
        whatsAppService.humanWait(10);
        if (!whatsAppService.isLoggedIn()) {
            String qrCodeSrc = whatsAppService.getScreenshot();
            System.out.println(qrCodeSrc);
            model.addObject("src", qrCodeSrc);
            model.addObject("authIsRequired", true);
        } else {
            model.addObject("authIsRequired", false);
        }
        return model;
    }

    @GetMapping("/send/{message}/to/{contact}")
    public ResponseEntity<String> sendMessage(@PathVariable(value = "contact") String contact, @PathVariable(value = "message") String message) {
        whatsAppService.refresh();
        boolean isSent = whatsAppService.sendMessage(contact, message);
        if (isSent)
            return ResponseEntity.ok("Message " + message + " was sent to " + contact + ".");
        else
            return ResponseEntity.of(Optional.of("Message wasn't sent"));
    }

}
