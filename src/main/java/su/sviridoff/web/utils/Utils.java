package su.sviridoff.web.utils;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



@Slf4j
public class Utils {

    public static boolean ifElementExists(WebDriver driver, By element) {
        return driver.findElements(element).size() == 1;

    }

    public static void waitForClickableAndClick(WebDriver driver, By element, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.ignoring(StaleElementReferenceException.class)
                .withMessage("Wait for clickable of the element: [" + element.toString() + "].")
                .until(ExpectedConditions.elementToBeClickable(element)).click();
    }

    public static void waitForPresence(WebDriver driver, By element, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.withMessage("Wait for presence of element: [" + element.toString() + "].")
                .until(ExpectedConditions.presenceOfElementLocated(element));
    }

    public static void waitForVisibility(WebDriver driver, By element, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.withMessage("Wait for visibility of element: [" + element.toString() + "].")
                .until(ExpectedConditions.visibilityOfElementLocated(element));
    }

}
