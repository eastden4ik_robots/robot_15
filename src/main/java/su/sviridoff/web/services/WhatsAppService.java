package su.sviridoff.web.services;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;
import su.sviridoff.web.utils.Utils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.logging.Level;

@Service
@Slf4j
public class WhatsAppService {

    @Value("${chromedriver.path}")
    private String chromedriverPath;
    @Value("${chromedriver.profile}")
    private String chromedriverProfile;
    @Value("${chromedriver.screenshot.folder}")
    private String chromedriverScreenshotFolder;
    @Value("${chromedriver.baseurl}")
    private String chromedriverBaseUrl;
    @Value("${chromedriver.title}")
    private String chromedriverTitle;

    public By mainPage = By.xpath(".//*[@id=\"side\"]");
    public By authPage = By.xpath(".//*[@aria-label=\"Scan me!\"]");
    public By newChatBtn = By.xpath(".//div[@title=\"Новый чат\"]");
    public By searchContactInput = By.xpath(".//div[text()=\"Поиск контактов\"]/following-sibling::label//div[@contenteditable]");
    public String contactResultXpath = ".//div[contains(@class,\"copyable-area\")]//span[@title=\"%s\"]";
    public By messageFieldInput = By.xpath(".//div[text()=\"Введите сообщение\"]/following-sibling::div");
    public By messageSentBtn = By.xpath(".//button/span[@data-icon=\"send\"]");
    public By sentMessageIsSentFlag = By.xpath("(//span[contains(@aria-label, \" Прочитано \") or contains(@aria-label, \" Доставлено \")])[last()]");
    public By contactNotFoundResult = By.xpath(".//span[contains(text(),\"Не найдено результатов\")]");

    private ChromeDriver driver = null;

    @SneakyThrows
    public  boolean sendMessage(String contact, String message) {
        boolean ifContactFound = getContact(contact);
        if (ifContactFound) {
            Utils.waitForVisibility(driver, messageFieldInput, 30);
            WebElement messageFieldInputElement = driver.findElement(messageFieldInput);
            String tempMessage = message.replaceAll("\n", Keys.chord(Keys.CONTROL, Keys.ENTER));
            messageFieldInputElement.sendKeys(tempMessage);
            Utils.waitForVisibility(driver, messageSentBtn, 30);
            WebElement messageSentBtnElement = driver.findElement(messageSentBtn);
            humanWait(5);
            messageSentBtnElement.click();
            Utils.waitForVisibility(driver, sentMessageIsSentFlag, 30);
            WebElement isMessageSentElement = driver.findElement(sentMessageIsSentFlag);
            if (isMessageSentElement.isDisplayed()) {
                log.info("Message: {}, was successfully sent to {}.", message, contact);
                return true;
            } else {
                log.error("Message: {} wasn't send to {}. :(", message, contact);
                return false;
            }
        } else {
            return false;
        }
    }

    @SneakyThrows
    public boolean getContact(String contact) {
        Utils.waitForVisibility(driver, newChatBtn, 30);
        WebElement newChatBtnElement = driver.findElement(newChatBtn);
        newChatBtnElement.click();
        Utils.waitForPresence(driver, searchContactInput, 30);
        WebElement searchContactInputElement = driver.findElement(searchContactInput);
        searchContactInputElement.sendKeys(contact);
        humanWait(5);
        if (!Utils.ifElementExists(driver, contactNotFoundResult)) {
            Utils.waitForVisibility(driver, By.xpath(String.format(contactResultXpath, contact)), 30);
            Utils.waitForClickableAndClick(driver, By.xpath(String.format(contactResultXpath, contact)), 30);
            Utils.waitForPresence(driver, messageFieldInput, 30);
            WebElement messageFieldInputElement = driver.findElement(messageFieldInput);
            if (messageFieldInputElement.isDisplayed()) {
                log.info("Contact successfully found. ;)");
                return true;
            } else {
                log.error("Not found result of the search {} in WhatsApp (Smartphone book)", contact);
                return false;
            }
        } else {
            return false;
        }
    }

    @SneakyThrows
    @PostConstruct @RequestScope(proxyMode = ScopedProxyMode.TARGET_CLASS)
    public void init() {
        System.setProperty("webdriver.chrome.driver", chromedriverPath);
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.addArguments("user-data-dir=" + chromedriverProfile);
        options.addArguments("--window-size=1366,768");
        driver = new ChromeDriver(options);
        driver.setLogLevel(Level.OFF);
    }

    @SneakyThrows
    public String getScreenshot() {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File dstFile = new File(chromedriverScreenshotFolder + "/" + getTitle() + ".png");
        BufferedImage originalImage = ImageIO.read(scrFile);

        log.info("Original image dimension: {}x{}", originalImage.getWidth(), originalImage.getHeight());
        BufferedImage subImage = originalImage.getSubimage(845, 135, 300, 300);
        log.info("Cropped image dimension: {}x{}", subImage.getWidth(), subImage.getHeight());

        if (dstFile.exists())
            if (dstFile.delete())
                log.info("Old screenshot [{}] was deleted.", dstFile.getAbsolutePath());
        ImageIO.write(subImage, "png", dstFile);
        return dstFile.getName();
    }

    public void open() {
        log.info("***************************************");
        log.info("Open web-site: {}", chromedriverBaseUrl);
        log.info("***************************************");
        driver.get(chromedriverBaseUrl);
        humanWait(15);
        WebDriverWait driverWait = new WebDriverWait(driver, 30);
        driverWait.withMessage("Waiting for opening web-site...").until(ExpectedConditions.titleIs(chromedriverTitle));
        log.info("Web-site {} was opened successfully!", chromedriverBaseUrl);
        log.info("***************************************");
    }

    public void refresh() {
        log.info("***************************************");
        driver.navigate().refresh();
        log.info("Page [{}] was refreshed.", driver.getTitle());
        log.info("***************************************");
    }

    public String getTitle() {
        return driver.getTitle();
    }

    public boolean isLoggedIn() {
        try {
            WebElement elem = driver.findElement(mainPage);
            if (elem != null) {
                log.info("WhatsApp is logged in.");
                return true;
            }
        } catch (NoSuchElementException ex) {
            log.error("WhatsApp is not logged in and authentication is required! {}.", ex.getMessage());
        }
        return false;
    }

    @PreDestroy
    public void destroy() {
        if (driver != null)
            driver.quit();
    }

    @SneakyThrows
    public void humanWait(int sec) {
        Thread.sleep(sec * 1000L);
    }

}
