package su.sviridoff.web.controllers;

import static org.mockito.AdditionalMatchers.or;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.isNull;
import static org.mockito.Mockito.when;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import su.sviridoff.web.services.WhatsAppService;

@ContextConfiguration(classes = {WhatsAppController.class})
@ExtendWith(SpringExtension.class)
public class WhatsAppControllerTest {
    @Autowired
    private WhatsAppController whatsAppController;

    @MockBean
    private WhatsAppService whatsAppService;

    @Test
    public void testGetCode() throws Exception {
        when(this.whatsAppService.isLoggedIn()).thenReturn(true);
        doNothing().when(this.whatsAppService).humanWait(anyInt());
        doNothing().when(this.whatsAppService).refresh();
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/whatsapp/QRCode");
        MockMvcBuilders.standaloneSetup(this.whatsAppController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.model().size(1))
                .andExpect(MockMvcResultMatchers.model().attributeExists("authIsRequired"))
                .andExpect(MockMvcResultMatchers.view().name("qr-code"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("qr-code"));
    }

    @Test
    public void testIsLoggedIn() throws Exception {
        when(this.whatsAppService.isLoggedIn()).thenReturn(true);
        doNothing().when(this.whatsAppService).humanWait(anyInt());
        doNothing().when(this.whatsAppService).refresh();
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/whatsapp/isLoggedIn");
        MockMvcBuilders.standaloneSetup(this.whatsAppController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=ISO-8859-1"))
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("WhatsApp service is logged in.")));
    }

    @Test
    public void testIsLoggedIn2() throws Exception {
        when(this.whatsAppService.isLoggedIn()).thenReturn(false);
        doNothing().when(this.whatsAppService).humanWait(anyInt());
        doNothing().when(this.whatsAppService).refresh();
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/whatsapp/isLoggedIn");
        MockMvcBuilders.standaloneSetup(this.whatsAppController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=ISO-8859-1"))
                .andExpect(MockMvcResultMatchers.content()
                        .string(Matchers.containsString("WhatsApp service is not logged in. Auth is required in whatsapp.")));
    }

    @Test
    public void testIsLoggedIn3() throws Exception {
        when(this.whatsAppService.isLoggedIn()).thenReturn(true);
        doNothing().when(this.whatsAppService).humanWait(anyInt());
        doNothing().when(this.whatsAppService).refresh();
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/whatsapp/isLoggedIn");
        getResult.contentType("Not all who wander are lost");
        MockMvcBuilders.standaloneSetup(this.whatsAppController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=ISO-8859-1"))
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("WhatsApp service is logged in.")));
    }

    @Test
    public void testSendMessage() throws Exception {
        when(this.whatsAppService.sendMessage(or(isA(String.class), isNull()), or(isA(String.class), isNull())))
                .thenReturn(true);
        doNothing().when(this.whatsAppService).refresh();
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/whatsapp/send/{message}/to/{contact}",
                "value", "value");
        MockMvcBuilders.standaloneSetup(this.whatsAppController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=ISO-8859-1"))
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("Message value was sent to value.")));
    }

    @Test
    public void testSendMessage2() throws Exception {
        when(this.whatsAppService.sendMessage(or(isA(String.class), isNull()), or(isA(String.class), isNull())))
                .thenReturn(false);
        doNothing().when(this.whatsAppService).refresh();
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/whatsapp/send/{message}/to/{contact}",
                "value", "value");
        MockMvcBuilders.standaloneSetup(this.whatsAppController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=ISO-8859-1"))
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("Message wasn't sent")));
    }

    @Test
    public void testSendMessage3() throws Exception {
        when(this.whatsAppService.sendMessage(or(isA(String.class), isNull()), or(isA(String.class), isNull())))
                .thenReturn(true);
        doNothing().when(this.whatsAppService).refresh();
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/whatsapp/send/{message}/to/{contact}",
                "value", "value");
        getResult.contentType("Not all who wander are lost");
        MockMvcBuilders.standaloneSetup(this.whatsAppController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=ISO-8859-1"))
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("Message value was sent to value.")));
    }

    @Test
    public void testGetCode2() throws Exception {
        when(this.whatsAppService.getScreenshot()).thenReturn("foo");
        when(this.whatsAppService.isLoggedIn()).thenReturn(false);
        doNothing().when(this.whatsAppService).humanWait(anyInt());
        doNothing().when(this.whatsAppService).refresh();
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/whatsapp/QRCode");
        MockMvcBuilders.standaloneSetup(this.whatsAppController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.model().size(2))
                .andExpect(MockMvcResultMatchers.model().attributeExists("authIsRequired", "src"))
                .andExpect(MockMvcResultMatchers.view().name("qr-code"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("qr-code"));
    }

    @Test
    public void testIndex() throws Exception {
        when(this.whatsAppService.getTitle()).thenReturn("foo");
        doNothing().when(this.whatsAppService).open();
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/whatsapp/open");
        MockMvcBuilders.standaloneSetup(this.whatsAppController)
                .build()
                .perform(requestBuilder)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=ISO-8859-1"))
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("Opened web-site: foo")));
    }

    @Test
    public void testIndex2() throws Exception {
        when(this.whatsAppService.getTitle()).thenReturn("foo");
        doNothing().when(this.whatsAppService).open();
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/whatsapp/open");
        getResult.contentType("Not all who wander are lost");
        MockMvcBuilders.standaloneSetup(this.whatsAppController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=ISO-8859-1"))
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("Opened web-site: foo")));
    }
}

